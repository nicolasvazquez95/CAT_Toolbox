# Readme

Este repositorio fue hecho para facilitar las herramientas necesarias (librerías) como un instructivo más sencillo de seguir que el original para poder compilar [Crystal Analysis Tool](https://gitlab.com/stuko/crystal-analysis-tool), creada por A. Stukowski. El código fuente llevaal menos 4 o 5 años sin mantenimiento, y esto dificulta bastante poder compilarlo con todas las librerías necesarias, que además se requieren versiones específicas.

En la carpeta `Libraries`, se encuentran comprimidas todas las versiones que el código original necesita para poder compilar correctamente, las cuales son:

* Zlib
* Eigen
* CGAL
* MPFR
* GMP

Además, se encontrará (próximamente) en la carpeta un tutorial que detalla paso a paso como realizar la descarga del código fuente desde el repositorio original de CAT, y la posterior compilación de cada una de las librerías necesarias por separado.

Espero que estas recetas sean de ayuda, tanto para mi yo del futuro, como para cualquiera que se vea en la necesidad de utilizar esta herramienta. 